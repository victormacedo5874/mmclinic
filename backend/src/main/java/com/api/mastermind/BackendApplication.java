package com.api.mastermind;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.api.mastermind.enuns.EntityType;
import com.api.mastermind.enuns.PersonType;
import com.api.mastermind.model.Address;
import com.api.mastermind.model.City;
import com.api.mastermind.model.Company;
import com.api.mastermind.model.Person;
import com.api.mastermind.model.Product;
import com.api.mastermind.model.State;
import com.api.mastermind.repository.CityRepository;
import com.api.mastermind.repository.CompanyRepository;
import com.api.mastermind.repository.PersonRepository;
import com.api.mastermind.repository.ProductRepository;
import com.api.mastermind.repository.StateRepository;

@SpringBootApplication
public class BackendApplication implements CommandLineRunner {

	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Value("${spring.profiles.active}")
	private String typeBase;
	
	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if(typeBase.equals("dev")) {
			inicitalStateAndCity();
			inicitalCompany();
			inicitalPerson();
			inicitalProduct();
		}
		
	}
	
	public void inicitalStateAndCity() {
		System.out.println("inicial State");
		
		State state1 = new State();
		state1.setId("eb3fbe4ce19947fcaeb74bb1eeabacaf");
		state1.setCodIbge("41");
		state1.setDescription("Paraná");
		state1.setInitials("PR");
		stateRepository.save(state1);
		
		State state2 = new State();
		state2.setId("9141026764a845fb89e1e72176dd1fc9");
		state2.setCodIbge("35");
		state2.setDescription("São Paulo");
		state2.setInitials("SP");
		stateRepository.save(state2);
		
		City city1 = new City();
		city1.setId("0b9a6f53378f48e183aa38fdc1480b34");
		city1.setDescription("Apucarana");
		city1.setIbge("4101408");
		city1.setState(state1);
		
		City city2 = new City();
		city2.setId("196783faef8b4f0a8f19a25f18233daa");
		city2.setDescription("Londrina");
		city2.setIbge("4113700");
		city2.setState(state1);
		
		City city3 = new City();
		city3.setId("17c1667468fb469d881062b5a0c8e6e8");
		city3.setDescription("São Paulo");
		city3.setIbge("3550308");
		city3.setState(state2);
		
		cityRepository.saveAll(Arrays.asList(city1, city2,city3));
		
	}
	
	public void inicitalCompany() {
		System.out.println("inicial Company");
		
		Optional<City> city = cityRepository.findById("0b9a6f53378f48e183aa38fdc1480b34");
		
		Company c1 = new Company();
		c1.setId("3efd1e607498416e9897d4303844b06f");
		c1.setCnpjCpf("56604240000150");
		c1.setPersonType("J");
		c1.setName("Lucca e Diogo Clinica Ltda");
		c1.setTradeName("Lucca's Clínica");
		c1.setEmail("lucas@gmail.com");
		c1.setPhone("4330475687");

		Address address = new Address();
		address.setId("2d847d5857494d8592592cbc64f06f7f");
		address.setCity(city.get());
		address.setNeighborhood("Centro");
		address.setStreet("Av. Curitiba");
		address.setStreetNumber("143");
		address.setZipcode("86800702");
		c1.setAddress(address);
		
		Company c2 = new Company();
		c2.setId("e920a08fd56d4fe4b87251b5759859d7");
		c2.setCnpjCpf("56604240000150");
		c2.setPersonType("J");
		c2.setName("Vanessa e Mario Clinica Ltda");
		c2.setTradeName("Clínica Beleza ");
		c2.setEmail("vanessa@gmail.com");
		c2.setPhone("4330479987");

		Address address2 = new Address();
		address2.setId("30a88ffe90e9483d91c8c1920a3d8fa6");
		address2.setCity(city.get());
		address2.setNeighborhood("Centro");
		address2.setStreet("Rua Ponta Grossa");
		address2.setStreetNumber("1433");
		address2.setComplement("Sobre Loja.");
		address2.setZipcode("86800030");
		c2.setAddress(address2);
		
		companyRepository.saveAll(Arrays.asList(c1, c2));
				
	}
	
	public void inicitalPerson() {
		System.out.println("inicial Person");
		
		Optional<City> city = cityRepository.findById("0b9a6f53378f48e183aa38fdc1480b34");
		Optional<Company> company = companyRepository.findById("e920a08fd56d4fe4b87251b5759859d7");
		
		Person p1 = new Person();
		p1.setId("3efd1e607498416e9897d4303844b033");
		p1.setCnpjCpf("10106124726");
		p1.setPersonType(PersonType.FISICA);
		p1.setEntityType(EntityType.CLIENT);
		p1.setName("Pedro Vidotto");
		p1.setTradeName("Pedro");
		p1.setEmail("pedro@gmail.com");
		p1.setPhone("43996512918");
		p1.setCompany(company.get());

		Address address = new Address();
		address.setId("22447d5857494d8592592cbc64f06f7f");
		address.setCity(city.get());
		address.setNeighborhood("Barra Funda");
		address.setStreet("Ponta Grossa");
		address.setStreetNumber("765");
		address.setZipcode("86800450");
		p1.setAddress(address);
		
		Person p2 = new Person();
		p2.setId("3efd1e607498416e9897d4303844b088");
		p2.setCnpjCpf("53700235658");
		p2.setPersonType(PersonType.FISICA);
		p2.setEntityType(EntityType.CLIENT);
		p2.setName("Victor Gabriel");
		p2.setTradeName("Victor");
		p2.setEmail("victor@gmail.com");
		p2.setPhone("43995378988");
		p2.setCompany(company.get());
		
		Address address2 = new Address();
		address2.setId("33447d5857494d8592592cbc64f06f7f");
		address2.setCity(city.get());
		address2.setNeighborhood("Vl. Regina");
		address2.setStreet("Mario Cazangi");
		address2.setStreetNumber("76");
		address2.setZipcode("86801660");
		p2.setAddress(address2);
		
		
		Person p3 = new Person();
		p3.setId("3efd1e607498416e9897d4303844b999");
		p3.setCnpjCpf("79644965000177");
		p3.setPersonType(PersonType.FISICA);
		p3.setEntityType(EntityType.PROVIDER);
		p3.setName("Joana e Antonella ME");
		p3.setTradeName("Joana");
		p3.setEmail("joana@gmail.com");
		p3.setPhone("4330357895");
		p3.setCompany(company.get());
		
		Address address3 = new Address();
		address3.setId("44447d5857494d8592592cbc64f06f7f");
		address3.setCity(city.get());
		address3.setNeighborhood("Vl. Regina");
		address3.setStreet("Mario Cazangi");
		address3.setStreetNumber("76");
		address3.setZipcode("86801660");
		p3.setAddress(address3);
		
		personRepository.saveAll(Arrays.asList(p1, p2, p3));
				
	}
	
	public void inicitalProduct() {
		System.out.println("inicial Product");
		
		Optional<Company> company = companyRepository.findById("e920a08fd56d4fe4b87251b5759859d7");
		Optional<Person> provider = personRepository.findById("3efd1e607498416e9897d4303844b999");
		BigDecimal valor = new BigDecimal(2);
		
		Product p1 = new Product();
		p1.setId("3efd1e607498416e9897d4303844b321");
		p1.setCompany(company.get());
		p1.setProvider(provider.get());
		p1.setDescription("Luva");
		p1.setValueSaleUse(valor);
		p1.setValueCost(valor);
		
		productRepository.save(p1);
				
	}
}
