package com.api.mastermind.DTO;
import javax.validation.constraints.NotEmpty;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;

@Data
@EqualsAndHashCode(callSuper = false)
public class AddressDTO extends AbstractBaseDTO {
 
	@NotEmpty(message = "Bairro é obrigatório")
    private String neighborhood;
	
	@NotEmpty(message = "Rua é obrigatório")
    private String street;
	
	@NotEmpty(message = "Número é obrigatório")
	private String streetNumber;
	
	@NotEmpty(message = "CEP é obrigatório")
	@Setter(AccessLevel.NONE)
	private String zipcode;
	
	private String complement;
	
	@NotEmpty(message = "É necessário informar a cidade")
	private CityDTO city;

	public void setZipcode(String zipcode) {
		zipcode = zipcode.replaceAll("([.-])", "");
		this.zipcode = zipcode;
	}
	
}
