package com.api.mastermind.DTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CityDTO extends AbstractBaseDTO {
 
	public String description;
	public String ibge;
}
