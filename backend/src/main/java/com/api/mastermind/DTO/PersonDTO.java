package com.api.mastermind.DTO;
import java.util.Objects;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;

@Data
@EqualsAndHashCode(callSuper = false)
public class PersonDTO extends AbstractBaseDTO {
 
	@NotBlank(message = "Nome é obrigatorio")
	private String name;
	private Long nr;
	@NotBlank(message = "Nome fantasia é obrigatorio")
	private String tradeName;
	@NotNull(message = "CNPJ/CPF é obrigatorio")
	@NotBlank(message = "CNPJ/CPF é obrigatorio")
	private String cnpjCpf;
	@NotNull(message = "Tipo é obrigatorio")
	@NotBlank(message = "Tipo é obrigatorio")
	private String personType;
	@NotNull(message = "Tipo Vinculo é obrigatorio")
	@NotBlank(message = "Tipo Vinculo é obrigatorio")
	private String entityType;
	@NotNull(message = "Telefone é obrigatorio")
	@NotBlank(message = "Telefone é obrigatorio")
	@Setter(AccessLevel.NONE)
	private String phone;
	private String email;
	private AddressDTO address;
	@NotNull(message = "Empresa não pode ser vazio")
	@NotBlank(message = "Empresa é Obrigatório")
	private String companyId;
	
	public void setCnpj(String cnpjCpf) {
		if(Objects.nonNull(cnpjCpf)) {
			cnpjCpf = cnpjCpf.replaceAll("([./-])", "");
		}
		this.cnpjCpf = cnpjCpf;
	}
	public void setPhone(String phone) {
		if(Objects.nonNull(phone)) {
			phone = phone.replaceAll("([()-])", "")
						 .replace(" ", "");
		}
		this.phone = phone;
	}
	
}
