package com.api.mastermind.DTO;
import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ProductDTO extends AbstractBaseDTO {
 
	@NotBlank(message = "Nome é obrigatorio")
	private String description;
	@NotNull(message = "Valor custo é obrigatório")
	private BigDecimal valueCost;
	@NotNull(message = "Valor de venda/uso é obrigatório")
	private BigDecimal valueSaleUse;
	@NotNull(message = "Fornecedor não pode ser vazio")
	private ProviderDTO provider;
	@NotNull(message = "Empresa não pode ser vazio")
	@NotBlank(message = "Empresa é Obrigatório")
	private String companyId;
	
}
