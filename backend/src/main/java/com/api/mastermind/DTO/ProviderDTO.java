package com.api.mastermind.DTO;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ProviderDTO extends AbstractBaseDTO {
 
	@NotBlank(message = "Nome é obrigatorio")
	private String name;
	@NotBlank(message = "Nome fantasia é obrigatorio")
	private String tradeName;
	@NotNull(message = "CNPJ/CPF é obrigatorio")
	@NotBlank(message = "CNPJ/CPF é obrigatorio")
	private String cnpjCpf;
	@NotNull(message = "Tipo é obrigatorio")
	@NotBlank(message = "Tipo é obrigatorio")
	private String personType;
	
}
