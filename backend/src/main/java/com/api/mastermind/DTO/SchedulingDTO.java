package com.api.mastermind.DTO;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SchedulingDTO extends AbstractBaseDTO{

	private String note;
	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private Long time;
	private String idCompany;
	private PersonDTO person;
	
}
