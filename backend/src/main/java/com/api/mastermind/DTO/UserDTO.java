package com.api.mastermind.DTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserDTO extends AbstractBaseDTO {
 
	private String name;

	private String login;
	
	private String password;
	
}
