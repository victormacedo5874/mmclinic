package com.api.mastermind.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.mastermind.model.City;
import com.api.mastermind.repository.CityRepository;

@RestController
@RequestMapping("city")
public class CityController {
	
	@Autowired
	CityRepository cityRepository;
	
	@GetMapping()
	public ResponseEntity<List<City>> getAll(){
		return new ResponseEntity<List<City>>(cityRepository.findAll(),HttpStatus.OK);
	}
	
	@GetMapping("byDescription")
	public ResponseEntity<List<City>> getCustomerByName(@RequestParam(name = "description") String description,
														@RequestParam(name = "uf") String uf,
														@RequestParam(name = "size", defaultValue = "5") int size){
		
		PageRequest pageable = PageRequest.of(0, size);
		List<City> citys = (uf != null && !uf.isEmpty()) ? cityRepository.findByUfAndDescriptionCity(uf, description, pageable) 
														 : cityRepository.findByDescriptionContaining(description,pageable);
		
		return new ResponseEntity<List<City>>(citys,HttpStatus.OK);
	}
	
}