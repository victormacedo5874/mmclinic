package com.api.mastermind.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.mastermind.DTO.CompanyDTO;
import com.api.mastermind.mapper.CompanyMapper;
import com.api.mastermind.model.Company;
import com.api.mastermind.service.CompanyService;
import com.fasterxml.jackson.core.JsonProcessingException;
	
@RestController
@RequestMapping("company")
public class CompanyController{
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	CompanyMapper companyMapper;
	
	@GetMapping
	public ResponseEntity<Iterable<CompanyDTO>> getAllCompany(){
		
		List<Company> comp = companyService.getAllCompany();
		List<CompanyDTO> compDTO = new ArrayList<CompanyDTO>();
		comp.stream().forEach(c ->{
			compDTO.add(companyMapper.toDTO(c));
		});
		
		return new ResponseEntity<Iterable<CompanyDTO>>(compDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> delegette(@PathVariable String id){
		return new ResponseEntity<>(companyService.getId(id), HttpStatus.OK);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable String id){
		companyService.delete(id);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@Transactional(rollbackOn = Exception.class)
	@PostMapping
	public ResponseEntity<CompanyDTO> save(@Valid @RequestBody CompanyDTO companyDTO)  {

		List<Company> companies = companyService.getAllCompany();
		companies.forEach(c->{
			if(Objects.isNull(companyDTO.getId())){
				if(companyDTO.getCnpjCpf().equals(c.getCnpjCpf()) ){
				 throw new RuntimeException("Cnpj/Cpf já cadastro.");
				}
			}else {
				if(!companyDTO.getId().equals(c.getId()) && companyDTO.getCnpjCpf().equals(c.getCnpjCpf()) ){
					throw new RuntimeException("Cnpj/Cpf já cadastro.");	
				}
			}
		});
		
		Company comp = companyService.save(companyMapper.toCompany(companyDTO));
		
		return new ResponseEntity<>(companyMapper.toDTO(comp),HttpStatus.CREATED);
	}
}