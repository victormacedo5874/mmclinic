package com.api.mastermind.controller;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.mastermind.DTO.PersonDTO;
import com.api.mastermind.mapper.PersonMapper;
import com.api.mastermind.model.Company;
import com.api.mastermind.model.Person;
import com.api.mastermind.service.PersonService;
import com.fasterxml.jackson.core.JsonProcessingException;
	
@RestController
@RequestMapping("person")
public class PersonController{
	
	@Autowired
	PersonService personService;
	
	@Autowired
	PersonMapper personMapper;
	
	@GetMapping
	public ResponseEntity<Page<PersonDTO>> findAllWithPageAndFilter(Pageable page,
																    @RequestParam(name = "filter") String filter,
																    @RequestParam(name = "companyId") String companyId,
																    @RequestParam(name = "entityType") String entityType) {
		Page<Person> personIterable = personService.findByPerson(companyId,  filter, page, entityType);
		return new ResponseEntity<Page<PersonDTO>>(personIterable.map(personMapper::toDTO), HttpStatus.OK);
	}
	
	@GetMapping("/provider/{id}")
	public ResponseEntity<?> provider(@PathVariable String id){
		List<Person> persons = personService.getEntityTaype(id, "Fornecedor");
		List<PersonDTO> personDTO = new ArrayList<PersonDTO>();
		persons.stream().forEach(p -> personDTO.add(personMapper.toDTO(p)));
		return new ResponseEntity<>(personDTO, HttpStatus.OK);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable String id){
		personService.delete(id);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping("find-by-document")
	public ResponseEntity<PersonDTO> findByCnpjCpf(@RequestParam("cnpjCpf") String cnpjCpf
												  ,@RequestParam(name = "idCompany") String idCompany){
		return new ResponseEntity<PersonDTO>(personMapper.toDTO(personService.findByCompanyAndCnpjCpf(new Company(idCompany), cnpjCpf)), HttpStatus.OK);
	}
	
	@Transactional(rollbackOn = Exception.class)
	@PostMapping
	public ResponseEntity<PersonDTO> save(@Valid @RequestBody PersonDTO personDTO) throws JsonProcessingException {

		Person person = personService.save(personMapper.toPerson(personDTO));
		
		return new ResponseEntity<>(personMapper.toDTO(person),HttpStatus.CREATED);
	}
	
}