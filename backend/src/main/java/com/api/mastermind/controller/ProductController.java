package com.api.mastermind.controller;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.mastermind.DTO.ProductDTO;
import com.api.mastermind.mapper.ProductMapper;
import com.api.mastermind.model.Product;
import com.api.mastermind.service.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
	
@RestController
@RequestMapping("product")
public class ProductController{
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ProductMapper productMapper;
	
	@GetMapping
	public ResponseEntity<Page<ProductDTO>> findAllWithPageAndFilter(Pageable page,
																    @RequestParam(name = "filter") String filter,
																    @RequestParam(name = "companyId") String companyId) {
		Page<Product> productIterable = productService.findByProductFilter(companyId,  filter, page);
		return new ResponseEntity<Page<ProductDTO>>(productIterable.map(productMapper::toDTO), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> delegette(@PathVariable String id){
		return new ResponseEntity<>(productService.getId(id), HttpStatus.OK);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable String id){
		productService.delete(id);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@Transactional(rollbackOn = Exception.class)
	@PostMapping
	public ResponseEntity<ProductDTO> save(@Valid @RequestBody ProductDTO productDTO,  BindingResult result) throws JsonProcessingException {

		if(result.hasErrors()) {
			throw new RuntimeException(result.getFieldError().getDefaultMessage());
		}
		
		Product comp = productService.save(productMapper.toProduct(productDTO));
		
		return new ResponseEntity<>(productMapper.toDTO(comp),HttpStatus.CREATED);
	}
}