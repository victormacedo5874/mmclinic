package com.api.mastermind.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.mastermind.DTO.SchedulingDTO;
import com.api.mastermind.mapper.SchedulingMapper;
import com.api.mastermind.service.SchedulingService;
	
@RestController
@RequestMapping("scheduling")
public class SchedulingController{
	
	@Autowired
	SchedulingService schedulingService;
	
	@Autowired
	SchedulingMapper mapper;
	
	@PostMapping
	public ResponseEntity<SchedulingDTO> save(@Valid @RequestBody SchedulingDTO schedulingDTO){
		return new ResponseEntity<>(mapper.toDTO(schedulingService.save(mapper.toScheduling(schedulingDTO))),HttpStatus.CREATED);
	}
	
	@GetMapping
	public ResponseEntity<List<SchedulingDTO>> findAll() {
		return new ResponseEntity<>(schedulingService.findByAll().stream().map(mapper::toDTO).collect(Collectors.toList()),HttpStatus.CREATED);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<SchedulingDTO> findById(@PathVariable("id") String id) {
		return new ResponseEntity<>(mapper.toDTO(schedulingService.findById(id)),HttpStatus.CREATED);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<SchedulingDTO> delete(@PathVariable("id") String id) {
		schedulingService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
}