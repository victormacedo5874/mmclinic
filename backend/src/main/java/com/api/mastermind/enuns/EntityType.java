package com.api.mastermind.enuns;

import java.util.Arrays;

import org.springframework.util.StringUtils;

import com.api.mastermind.error.EnumNotFoundException;

public enum EntityType {
	CLIENT("Cliente"), PROVIDER("Fornecedor");

	private String description;

	private EntityType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public EntityType toEnum(String status) {
		if (!StringUtils.hasLength(status)) {
			return null;
		}

		try {
			return Arrays.asList(EntityType.values()).stream().filter(value -> value.description.equals(status))
					.findFirst().get();
		} catch (Exception e) {
			throw new EnumNotFoundException("Não encontrado valor para " + status + " EntityType");
		}

	}

}
