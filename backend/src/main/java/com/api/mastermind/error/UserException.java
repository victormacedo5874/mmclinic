package com.api.mastermind.error;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class UserException extends AuthenticationException {
	private static final long serialVersionUID = 1L;

	public UserException(String message) {
		super(message);
	}
	
}