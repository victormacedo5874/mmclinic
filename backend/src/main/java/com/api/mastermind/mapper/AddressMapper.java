package com.api.mastermind.mapper;

import org.mapstruct.Mapper;

import com.api.mastermind.DTO.AddressDTO;
import com.api.mastermind.model.Address;

@Mapper
public interface AddressMapper {

	public AddressDTO toDTO(Address address);
	public Address toAddress(AddressDTO addressDTO); 
	
}

