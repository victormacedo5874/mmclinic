package com.api.mastermind.mapper;

import org.mapstruct.Mapper;

import com.api.mastermind.DTO.CompanyDTO;
import com.api.mastermind.model.Company;

@Mapper(uses = AddressMapper.class)
public interface CompanyMapper {

	public CompanyDTO toDTO(Company company);
	public Company toCompany(CompanyDTO company); 
	
}
