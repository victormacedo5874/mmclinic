package com.api.mastermind.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.api.mastermind.DTO.PersonDTO;
import com.api.mastermind.model.Person;

@Mapper(uses = AddressMapper.class)
public interface PersonMapper {

	@Mappings(value = {
			@Mapping(source = "company.id",target = "companyId")
	})
	public PersonDTO toDTO(Person person);
	
	@Mappings(value = {
			@Mapping(source = "companyId", target = "company.id")
	})
	public Person toPerson(PersonDTO person);
	
	
}
