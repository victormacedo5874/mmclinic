package com.api.mastermind.mapper;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.util.StringUtils;

import com.api.mastermind.DTO.AbstractBaseDTO;
import com.api.mastermind.annotation.ValidNull;
import com.api.mastermind.model.AbstractBaseEntity;

@Mapper
public abstract class ProcessMapper {

	 @AfterMapping
     public void validNullObjects(Object baseDTO, @MappingTarget Object baseEntity){ 
		 if(baseEntity instanceof AbstractBaseDTO) {
			 return;
		 }
		 
		 Predicate<Field> predicate = field -> {
			 ValidNull v = field.getDeclaredAnnotation(ValidNull.class);
			 return v != null;
		 };
		 
		 Consumer<Field> consumer = field ->{
			 try {
				ValidNull validNull = field.getDeclaredAnnotation(ValidNull.class);
				Field f = baseEntity.getClass().getDeclaredField(validNull.source());
				
				Method methodGet = baseEntity.getClass().getDeclaredMethod(getMethodByFieldName("get",validNull.source()));
				
				AbstractBaseEntity entity = (AbstractBaseEntity) methodGet.invoke(baseEntity);
				
				if(!StringUtils.hasLength(entity.getId())) {
					return;
				}
				
				Method methodSet = baseEntity.getClass().getDeclaredMethod(getMethodByFieldName("set",validNull.source()), f.getType());
				AbstractBaseEntity ab = null;
				methodSet.invoke(baseEntity, ab);
			} catch (Exception e) {
			}
		 };
		 
		  Arrays.asList(baseDTO.getClass().getDeclaredFields()).stream()
		  											   .filter(predicate)
		  											   .forEach(consumer);
     }
	 
	 private String getMethodByFieldName(String method, String field) {
		 return String.format("%s%s", method, field.substring(0, 1).toUpperCase() + field.substring(1));
	 }
}
