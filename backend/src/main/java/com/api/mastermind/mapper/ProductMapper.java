package com.api.mastermind.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.api.mastermind.DTO.ProductDTO;
import com.api.mastermind.model.Product;

@Mapper(uses = PersonMapper.class)
public interface ProductMapper {

	@Mappings(value = {
			@Mapping(source = "company.id",target = "companyId")
	})
	public ProductDTO toDTO(Product product);
	
	@Mappings(value = {
			@Mapping(source = "companyId", target = "company.id")
	})
	public Product toProduct(ProductDTO product); 
	
}
