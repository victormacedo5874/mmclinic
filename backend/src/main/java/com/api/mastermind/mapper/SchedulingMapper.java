package com.api.mastermind.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.api.mastermind.DTO.SchedulingDTO;
import com.api.mastermind.model.Scheduling;

@Mapper(uses = PersonMapper.class)
public interface SchedulingMapper {

	@Mappings(value = {
			@Mapping(source = "company.id",target = "idCompany")
	})
	public SchedulingDTO toDTO(Scheduling scheduling);
	
	@Mappings(value = {
			@Mapping(source = "idCompany", target = "company.id")
	})
	public Scheduling toScheduling(SchedulingDTO dto); 
	
}
