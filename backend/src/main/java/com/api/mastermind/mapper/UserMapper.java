package com.api.mastermind.mapper;

import com.api.mastermind.DTO.UserDTO;
import com.api.mastermind.model.User;

public interface UserMapper {

	public UserDTO toDTO(User user);
	public User toUser(UserDTO userDTO); 
}
