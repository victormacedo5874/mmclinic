package com.api.mastermind.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.annotations.Where;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@MappedSuperclass
@Data
@Where(clause = "delete_date is null")
public abstract class AbstractBaseEntity {
 
	@Id
	private String id;
	@Column(name = "inclusion_date", updatable = false)
	private Date inclusionDate;
	@Column(name = "delete_date")
	@JsonIgnore
	private Date deleteDate;
	@Column(name = "update_date")
	@JsonIgnore
	private Date updateDate;
 
	@SuppressWarnings("deprecation")
	@PrePersist
	public void prePersist() {
		if(StringUtils.isEmpty(this.id)) {
			this.id = UUID.randomUUID().toString().replaceAll("-", "");
			this.inclusionDate = new Date();
		}
	}
	
	@PreUpdate
	public void preUpdate() {
		this.updateDate = new Date();
	}
}
