package com.api.mastermind.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Address extends AbstractBaseEntity  {
	
	@Column(length = 60, nullable = false)
    private String neighborhood;
	
	@Column(length = 120, nullable = false)
    private String street;
	
	@Column(length = 120, nullable = false)
	private String streetNumber;
	
	@Column(length = 10, nullable = false)
	private String zipcode;
	
	@Column(length = 250, nullable = true)
	private String complement;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_city")
	private City city;

}
