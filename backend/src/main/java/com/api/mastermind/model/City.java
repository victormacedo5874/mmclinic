package com.api.mastermind.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data 
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class City extends AbstractBaseEntity {

	@Column(length = 60, nullable = false)
    private String description;
	
	@Column(length = 7, nullable = false)
	private String ibge;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_state")
	private State state;
	
}

