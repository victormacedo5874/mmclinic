package com.api.mastermind.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Company extends AbstractBaseEntity{

	@Column(length = 120, nullable = false)
	private String name;
	@Column(length = 120, nullable = false)
	private String tradeName;
	@Column(length = 20, nullable = false)
	private String cnpjCpf;
	@Column(length = 20, nullable = true)
	private String personType;
	@Column(length = 20, nullable = true)
	private String phone;
	@Column(length = 50, nullable = true)
	private String email;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_address")
	private Address address;
	
	public Company() {
		
	}
	public Company(String id) {
		super.setId(id);
	}
}
