package com.api.mastermind.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.api.mastermind.enuns.EntityType;
import com.api.mastermind.enuns.PersonType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Person extends AbstractBaseEntity{

	@Column
	private Long nr;
	@Column(length = 20, nullable = false)
	private EntityType entityType;
	@Column(length = 120, nullable = false)
	private String name;
	@Column(length = 120)
	private String tradeName;
	@Column(length = 20, nullable = false)
	private String cnpjCpf;
	@Column(length = 20)
	private PersonType personType;
	@Column(length = 20)
	private String phone;
	@Column(length = 50)
	private String email;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_address")
	private Address address;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_company", updatable = false, nullable = false)
	private Company company;
	
	public Person() {
		
	}
}
