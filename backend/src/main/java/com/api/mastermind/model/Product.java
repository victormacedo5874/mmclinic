package com.api.mastermind.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Product extends AbstractBaseEntity{

	@Column(length = 120, nullable = false)
	private String description;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "provider_id")
    private Person provider;
    @Column(length = 12, precision=2)
	private BigDecimal valueCost;
    @Column(length = 12, precision=2)
	private BigDecimal valueSaleUse;
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_company", updatable = false, nullable = false)
	private Company company;
	
	public Product() {
		
	}
}
