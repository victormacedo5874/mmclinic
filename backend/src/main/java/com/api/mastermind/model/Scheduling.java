package com.api.mastermind.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Scheduling extends AbstractBaseEntity{

	@Column(length = 200, nullable = false)
	private String note;
	
	private LocalDateTime startDate;
	
	private LocalDateTime endDate;
	
	private Long time;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_company", updatable = false, nullable = false)
	private Company company;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_person", updatable = false, nullable = false)
	private Person person;
	
}
