package com.api.mastermind.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class State extends AbstractBaseEntity {

	@Column(length = 60, nullable = true)
	private String description;

	@Column(length = 3, nullable = true)
	private String initials;

	@Column(length = 3, nullable = true, name = "cod_ibge")
	private String codIbge;

}
