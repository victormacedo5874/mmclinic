package com.api.mastermind.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class User extends AbstractBaseEntity {

	@Column(length = 30, nullable = false)
	private String name;

	@Column(length = 10, nullable = false)
	private String login;

	@Column(length = 50)
	private String email;

	@Column(length = 250)
	private String password;

	@Column
	private Boolean active;

//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "id_profile")
//	private Profile profile;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date registrationDate;

	@PrePersist
	public void prePersistUser() {
		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
		this.password = bCrypt.encode(this.password);
		this.registrationDate = new Date();
	}

}