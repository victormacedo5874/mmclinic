package com.api.mastermind.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.mastermind.model.Address;

public interface AddressRepository extends JpaRepository<Address, String> {
   
}
