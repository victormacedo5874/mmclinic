package com.api.mastermind.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.api.mastermind.model.City;

public interface CityRepository extends JpaRepository<City, String> {
   
	public Optional<City> findByDescription(String description);
	@Query("Select a from City a "
			+ "     where a.state.initials = :initials "
			+ "       and a.description like %:description%")
	public List<City> findByUfAndDescriptionCity(@Param("initials") String initial,@Param("description") String description, Pageable pageable);
	public List<City> findByDescriptionContaining(String description, Pageable pageable);
    
}
