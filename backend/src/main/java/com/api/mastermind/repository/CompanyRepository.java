package com.api.mastermind.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.mastermind.model.Company;

public interface CompanyRepository extends JpaRepository<Company, String> {
   
}
