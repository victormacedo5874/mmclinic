package com.api.mastermind.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.api.mastermind.enuns.EntityType;
import com.api.mastermind.model.Company;
import com.api.mastermind.model.Person;

public interface PersonRepository extends JpaRepository<Person, String> {
   

	@Query("select a "
		  + " from Person a "
		  + " join a.company b "
		  + "where b.id = :companyId"
		  + "  and (a.name like %:filter% or a.tradeName like %:filter% or a.cnpjCpf like %:filter%)"
		  + "  and a.entityType = :entityType"
		  +" order by a.name")
	Page<Person> findByPersonFilter(@Param("companyId") String companyId, @Param("filter") String filter, @Param("entityType") EntityType entityType, Pageable paging);
	
	Person findByCompanyAndCnpjCpf(Company company, String cnpjCpf);
	List<Person> findByCompanyAndEntityType(Company company, String type);
	
	@Query("select coalesce(max(a.nr),0) + 1 from Person a where a.company.id = :companyId")
	Long findNextNrByDay(@Param("companyId") String companyId);
	
	@Query("select case when count(a) > 0 then true else false end "
		  + " from Person a "
		  + " join a.company b "
		  + "where b.id = :companyId"
		  + "  and a.cnpjCpf = :cnpjCpf"
		  + "  and a.id <> :idPerson "
		  +" order by a.name")
	Boolean documentExists(@Param("companyId") String companyId, @Param("cnpjCpf") String cnpjCPF, @Param("idPerson") String idPerson);
		

}
