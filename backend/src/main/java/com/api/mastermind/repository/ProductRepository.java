package com.api.mastermind.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.api.mastermind.model.Product;

public interface ProductRepository extends JpaRepository<Product, String> {
   
	@Query("select a "
		  + " from Product a "
		  + " join a.company b "
		  + "where b.id = :companyId"
		  + "  and a.description like %:filter%"
		  +" order by a.description")
	Page<Product> findByProductFilter(@Param("companyId") String companyId, @Param("filter") String filter, Pageable paging);
	
}
