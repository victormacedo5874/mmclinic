package com.api.mastermind.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.mastermind.model.Scheduling;

public interface SchedulingRepository extends JpaRepository<Scheduling, String> {
   
	
}
