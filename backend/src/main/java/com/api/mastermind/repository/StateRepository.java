package com.api.mastermind.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.mastermind.model.State;

public interface StateRepository extends JpaRepository<State, String> {
   
	public Optional<State> findByInitials(String initials);
    
}
