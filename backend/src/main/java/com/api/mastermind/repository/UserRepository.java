package com.api.mastermind.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.mastermind.model.User;

public interface UserRepository extends JpaRepository<User, String> {
    public Optional<User> findByLogin(String login);
    
//    @Query("select a from User a join a.parckings b where b.id = :idParcking and a.id = :idUser")
//	public User findUserParcking(@Param(value = "idParcking") String idParcking, @Param(value = "idUser") String idUser);
}
