package com.api.mastermind.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.api.mastermind.DTO.UserDTO;
import com.api.mastermind.error.ErrorDetails;
import com.api.mastermind.error.UserException;
import com.api.mastermind.mapper.UserMapper;
import com.api.mastermind.model.User;
import com.api.mastermind.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
 
	private JWTUtil jwtUtil;
	
	private UserRepository userRepository;
	
	private UserMapper userMapper;
	
	private ObjectMapper mapper;
	
    public JWTAuthenticationFilter(JWTUtil jwtUtil, UserRepository userRepository, UserMapper userMapper) {
    	setAuthenticationFailureHandler(new JWTAuthenticationFailureHandler());
        this.jwtUtil = jwtUtil;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.mapper = new ObjectMapper();
    }
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			UserDTO user = new ObjectMapper()
	                .readValue(request.getInputStream(), UserDTO.class);
			
	        Authentication auth = this.authenticate(user);
	        return auth;
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private Authentication authenticate(UserDTO user) throws AuthenticationException {

		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
		Optional<User> userDB = userRepository.findByLogin(user.getLogin());
		
		
		if(!userDB.isPresent()) {
			throw new UserException("Usúario não encontrado");
		}
		
		if(!bCrypt.matches(user.getPassword(), userDB.get().getPassword())) {
			throw new UserException("Senha inválida");
		}
	    return new UsernamePasswordAuthenticationToken(userDB.get(), null, new ArrayList<>());
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
		Authentication authResult) throws IOException, ServletException {
		User user = (User) authResult.getPrincipal();
        String token = jwtUtil.generateToken(user.getLogin());
        
        response.setContentType("application/json;charset=utf-8"); 
        response.addHeader("Authorization", "Bearer " + token);
        response.addHeader("Access-Control-Expose-Headers", "Authorization");
        response.addHeader("Access-Control-Allow-Headers", "Authorization, X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, X-Custom-header");
        response.getWriter().append(mapper.writeValueAsString(userMapper.toDTO(user)));
	}
	
	private class JWTAuthenticationFailureHandler implements AuthenticationFailureHandler {
		 
        @Override
        public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
                throws IOException, ServletException {
//            response.setStatus(401);
//            response.setContentType("application/json"); 
//            ErrorDetails error = ErrorDetails.builder()
//				   				 .status(HttpStatus.UNAUTHORIZED.value())
//				   				 .message(e.getLocalizedMessage())
//				   				 .title("Request Error")
//				   				 .timestamp(new Date().getTime())
//				   				 .build();
//            response.getWriter().append(mapper.writeValueAsString(error));
        }
    }
	
	
}
