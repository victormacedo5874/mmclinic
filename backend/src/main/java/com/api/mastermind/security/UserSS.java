package com.api.mastermind.security;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserSS implements UserDetails{

	private static final long serialVersionUID = 1L;
	private String id;
	private String user;
	private String password;
	private Collection<? extends GrantedAuthority> authorities;
	
	public String getId() {
		return id;
	}
	
	public UserSS(String id, String user, String password, Set<?> access) {
		this.id = id;
		this.user = user;
		this.password = password;
		this.authorities = access.stream().map(a -> new SimpleGrantedAuthority("ALL")).collect(Collectors.toList());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.user;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
