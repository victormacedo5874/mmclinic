package com.api.mastermind.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.api.mastermind.model.Company;
import com.api.mastermind.repository.CompanyRepository;

@Service
public class CompanyService {

	@Autowired
	CompanyRepository companyRepository;
	
	public List<Company> getAllCompany(){
		return companyRepository.findAll();
	}
	
	public Company getId(String id){
		Optional<Company> comp = companyRepository.findById(id);
		return comp.get(); 
	}
	
	public ResponseEntity<?> delete(@PathVariable String id){
		companyRepository.deleteById(id);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	public Company save(@Valid @RequestBody Company company) {

		return companyRepository.save(company);
	}
}
