package com.api.mastermind.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.mastermind.enuns.EntityType;
import com.api.mastermind.model.Company;
import com.api.mastermind.model.Person;
import com.api.mastermind.repository.CompanyRepository;
import com.api.mastermind.repository.PersonRepository;

@Service
public class PersonService {

	@Autowired
	PersonRepository personRepository;
	
	@Autowired
	CompanyRepository companyRepository;
	
	public Page<Person> findByPerson(String companyId,  String filter, Pageable pageable, String entityType) {
		if(entityType.equals("Fornecedor")) {
			return personRepository.findByPersonFilter(companyId,filter,  EntityType.PROVIDER, pageable); 
		}else {
			return personRepository.findByPersonFilter(companyId,filter,EntityType.CLIENT,  pageable); 
		}
	}
	
	public List<Person> getAllPerson(){
		return personRepository.findAll();
	}
	
	public List<Person> getEntityTaype(String companyId, String type){
		Optional<Company> company = companyRepository.findById(companyId);
		return personRepository.findByCompanyAndEntityType(company.get(), type);
	}
	
	public Person getId(String id){
		Optional<Person> comp = personRepository.findById(id);
		return comp.get(); 
	}
	
	public void delete(String id){
		personRepository.deleteById(id);
	}
	
	public Person findByCompanyAndCnpjCpf(Company company, String cnpjCpf) {
		return personRepository.findByCompanyAndCnpjCpf(company, cnpjCpf);
	}
	
	public Person save(Person person) {
		Boolean documentExists = this.documentExists(person.getCompany().getId(), person.getCnpjCpf(), Optional.ofNullable(person.getId()).orElse(""));
		if(documentExists) {
			throw new RuntimeException("CPF/CNPJ já cadastrado no sistema");
		}
		return personRepository.save(person);
	}
	
	public Boolean documentExists(String companyId, String cnpjCpf, String idPerson) {
		return personRepository.documentExists(companyId, cnpjCpf, idPerson);
	}
	
}
