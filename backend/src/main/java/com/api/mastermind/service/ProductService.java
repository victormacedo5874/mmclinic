package com.api.mastermind.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.api.mastermind.model.Product;
import com.api.mastermind.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;
	
	public List<Product> getAllProduct(){
		return productRepository.findAll();
	}
	
	public Page<Product> findByProductFilter(String companyId,  String filter, Pageable pageable) {
		return productRepository.findByProductFilter(companyId,filter, pageable); 
	}
	
	public Product getId(String id){
		Optional<Product> comp = productRepository.findById(id);
		return comp.get(); 
	}
	
	public ResponseEntity<?> delete(@PathVariable String id){
		productRepository.deleteById(id);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	public Product save(@Valid @RequestBody Product product) {

		return productRepository.save(product);
	}
}
