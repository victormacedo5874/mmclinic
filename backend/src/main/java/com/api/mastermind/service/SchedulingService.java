package com.api.mastermind.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.mastermind.model.Person;
import com.api.mastermind.model.Scheduling;
import com.api.mastermind.repository.SchedulingRepository;

@Service
public class SchedulingService {

	@Autowired
	private SchedulingRepository schedulingRepository;
	
	@Autowired
	private PersonService personService;
	
	public Scheduling save(Scheduling scheduling) {
		Person person = scheduling.getPerson();
		person.setCompany(scheduling.getCompany());
		
		person = personService.save(person);
		scheduling.setPerson(person);
		
		return schedulingRepository.save(scheduling);
	}
	
	public Scheduling findById(String id) {
		return schedulingRepository.getOne(id);
	}
	
	public List<Scheduling> findByAll() {
		return schedulingRepository.findAll();
	}
	
	public void delete(String id) {
		schedulingRepository.deleteById(id);
	}
	
}
