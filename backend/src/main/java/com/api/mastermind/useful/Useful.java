package com.api.mastermind.useful;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.text.MaskFormatter;

import org.springframework.util.StringUtils;

public class Useful {

	public static Integer bigDecimalToInt(BigDecimal bigDecimal) {
		bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		return Integer.valueOf(bigDecimal.toString().replace(".", ""));
	}

	public static BigDecimal intToBigDecimal(Integer integer) {
		if(integer == 0 || integer.equals(0)) 
			return new BigDecimal("0");
		
		String aux = integer.toString();
		String bdAux = aux;
		if (aux.length() > 2) { 
			bdAux = aux.substring(0, aux.length() - 2) + "." + aux.substring(aux.length() - 2, aux.length());
		}else if(aux.length() == 2) {
			bdAux = "0." + aux;
		} else if (aux.length() == 1) {
			bdAux = "0.0" + aux;
		}
		return new BigDecimal(bdAux);
	}

	public static LocalDateTime convertToLocalDateTime(Date dateToConvert) {
	    return dateToConvert.toInstant()
	      .atZone(ZoneId.systemDefault())
	      .toLocalDateTime();
	}
	
	public static String formatPhone(String phone) throws ParseException {
		if(!StringUtils.hasLength(phone)) {
			return "";
		}
		
		MaskFormatter maskPhone;
		switch (phone.length()) {
		case 12:
			maskPhone = new MaskFormatter("(###) # ####-####");
			break;
		case 11:
			maskPhone = new MaskFormatter("(##) # ####-####");
			break;
		case 10:
			maskPhone = new MaskFormatter("(##) ####-####");
			break;
		default:
			return phone;
//			throw new RuntimeException("Formato de telefone não configurado");
		}
		maskPhone.setValueContainsLiteralCharacters(false);
		return maskPhone.valueToString(phone);
	}
	
}
