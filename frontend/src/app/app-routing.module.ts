import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'calendar', loadChildren: () => import('./pages/calendar/calendar.module').then(m => m.CalendarModule) },
  { path: 'company', loadChildren: () => import('./view/company/company.module').then(m => m.CompanyModule)},
  { path: 'person/client', loadChildren: () => import('./view/person/person.module').then(m => m.PersonModule)},
  { path: 'person/provider', loadChildren: () => import('./view/person/person.module').then(m => m.PersonModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
