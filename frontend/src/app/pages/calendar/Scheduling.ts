import { Person } from "../person/Person";

export class Scheduling{
	id:string;
	note:string;
	startDate:Date;
	endDate:Date;
	hour:number;
	time:number;
	idCompany:string;
	person:Person = new Person();
}