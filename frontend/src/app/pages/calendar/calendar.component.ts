import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Calendar, CalendarOptions, FullCalendarComponent } from '@fullcalendar/angular';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ModalEventComponent } from './modal-event/modal-event.component';
import { Scheduling } from './Scheduling';
import { SchedulingService } from './scheduling.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  @ViewChild('calendar', { static: true }) calendarComponent: FullCalendarComponent;
  apiCalendar:Calendar;
  modalRef: BsModalRef;

  form:FormGroup
  constructor(private modalService: BsModalService, private fb:FormBuilder, private schedulingService:SchedulingService) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.schedulingService.findAll().subscribe((schedulings:Scheduling[]) =>{
      schedulings.map(this.createEventFromScheduling).forEach(event => this.calendarComponent.getApi().addEvent(event));
    })
    
    this.calendarComponent.getApi().on('eventClick', (click)=>{
      let {id} = click.event;
      this.modalRef = this.modalService.show(ModalEventComponent,
        {
          class: 'modal-lg',
          initialState: { id },
        }
      );
      this.modalRef.content.onClose.subscribe((result:Scheduling) => {
        this.calendarComponent.getApi().getEventById(id).remove();
        if(result){
          this.calendarComponent.getApi().addEvent(this.createEventFromScheduling(result));
        }
      })
    })
    this.calendarComponent.getApi().on('dateClick', (event)=>{
      this.modalRef = this.modalService.show(ModalEventComponent,
        {
          class: 'modal-lg',
          initialState: { date: event.date },
        }
      );
      this.modalRef.content.onClose.subscribe((result:Scheduling) => {
        this.calendarComponent.getApi().addEvent(this.createEventFromScheduling(result));
      })
    })
  }

  createEventFromScheduling(scheduling:Scheduling){
    return { id:scheduling.id, title: scheduling.person.name, date: scheduling.startDate, end: scheduling.endDate }
  }

  mouseOver(event){
    console.log(event);
  }

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    locale: 'pt-br',
    selectable: true,
    nowIndicator: true,
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    buttonText: {
      today: 'Hoje',
      month: 'Mês',
      week: 'Semana',
      day: 'Dia',
      list: 'Lista'
    },
    allDayText: '',
  };

  teste(){}
}
