import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import timeGridPlugin from '@fullcalendar/timegrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction';

import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarComponent } from './calendar.component';
import { ModalEventComponent } from './modal-event/modal-event.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { SchedulingService } from './scheduling.service';
import { HttpClientModule } from '@angular/common/http';
import { PersonService } from '../person/person.service';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  timeGridPlugin,
  interactionPlugin
]);

const maskConfig: Partial<IConfig> = {
  validation: true,
};

@NgModule({
  declarations: [CalendarComponent, ModalEventComponent],
  imports: [
    CommonModule,
    CalendarRoutingModule,
    FullCalendarModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    NgxMaskModule.forRoot(maskConfig),
  ],
  providers:[SchedulingService, PersonService]
})
export class CalendarModule { }
