import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common' 
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject, Subscription } from 'rxjs';
import { SchedulingService } from '../scheduling.service';
import { Scheduling } from '../Scheduling';
import { PersonService } from '../../person/person.service';
import { Person } from '../../person/Person';
import { debounceTime } from 'rxjs/operators';


@Component({
  selector: 'app-modal-event',
  templateUrl: './modal-event.component.html',
  styleUrls: ['./modal-event.component.css'],
})
export class ModalEventComponent implements OnInit {
  date?:Date
  id?:string;
  onClose: Subject<any>;
  form:FormGroup
  constructor(private fb:FormBuilder, public modalRef: BsModalRef, private schedulingService:SchedulingService, private personService:PersonService) { }

  ngOnInit(): void {
    this.onClose = new Subject();
    this.form = this.fb.group({
      id: [''],
      startDate:[this.date ? formatDate(this.date,'yyyy-MM-dd','en') : '', Validators.required],
      hour:[this.date ? formatDate(this.date,'HH:mm','en') : '', Validators.required],
      time:['', [Validators.required, Validators.min(5)]],
      note:[''],
      person:this.fb.group({
        id: [''],
        name:['',[ Validators.required, Validators.minLength(3) ]],
        cnpjCpf:['',[ Validators.required, Validators.minLength(11)]],
        phone:['',[ Validators.required, Validators.minLength(11)]],
        email:[''],
        entityType: ["CLIENT"],
        personType: ["FISICA"],
        companyId:[''],
      })
    })

    if(this.id){
      this.loadScheduling();
    }
    
    this.initObservablePersonDocument();

  }

  loadScheduling(){
    this.schedulingService.findById(this.id).subscribe((scheduling:Scheduling) =>{
      this.form.patchValue(scheduling);
      this.form.get('startDate').setValue(formatDate(scheduling.startDate,'yyyy-MM-dd','en'));
      this.form.get('hour').setValue(formatDate(scheduling.startDate,'HH:mm','en'));
    })
  }

  initObservablePersonDocument(){
    this.form.get('person').get('cnpjCpf').valueChanges
    .subscribe((value:string) =>{
      if(value.length == 11){
        this.personService.findByDocument(value).subscribe((person:Person) =>{
          if(!person){
            this.form.get('person').patchValue({id : null}, {emitEvent: false});
            return;
          }
          this.form.get('person').patchValue(person, {emitEvent: false});
        })
      }
    })
  }

  delete(){
    this.schedulingService.delete(this.id).subscribe(() =>{
      this.onClose.next(null);
      this.modalRef.hide()
    })
  }

  submit(){
    if(this.form.invalid){
      return;
    }
    const startDate = new Date(`${this.form.value.startDate} ${this.form.value.hour}`)
    const endDate = new Date(new Date(startDate).setMinutes(startDate.getMinutes() + this.form.value.time))
    this.form.get('startDate').setValue(startDate);
    this.form.addControl('endDate',this.fb.control(endDate));
    this.schedulingService.save(this.form.value).subscribe(data =>{
      this.onClose.next(data);
      this.modalRef.hide()
    })
  }

}
