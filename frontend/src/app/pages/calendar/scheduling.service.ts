import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { endPoint } from 'src/main';
import { Scheduling } from './Scheduling';

@Injectable({
  providedIn: 'root'
})
export class SchedulingService {
  url =`${endPoint}`+"scheduling";
  isLoading = false;

  constructor(private http: HttpClient) {}

  showMessage(msg: string, isError: boolean = false): void {
    console.log(msg)
    // this.snackBar.open(msg, "X", {
    //   duration: 2000,
    //   horizontalPosition: "center",
    //   verticalPosition: "bottom",
    //   panelClass: isError ? ["msg-error"] : ["msg-success"],
    // });
  }

  findAll(): Observable<Scheduling[]> {
    return this.http.get<Scheduling[]>(this.url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  findById(id:string): Observable<Scheduling> {
    return this.http.get<Scheduling>(`${this.url}/${id}`).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  delete(id:string): Observable<any> {
    return this.http.delete(`${this.url}/${id}`).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  save(scheduling:Scheduling): Observable<Scheduling> {
    scheduling.idCompany = '3efd1e607498416e9897d4303844b06f';
    return this.http.post<Scheduling>(this.url, scheduling).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  errorHandler(e: any): Observable<any> {
    console.log(e)
    if(e.error.message){
      this.showMessage(e.error.message, true);
      return EMPTY;
    }
    if(e.error && e.error.fieldsError){
      for(let i in e.error.fieldsError){
        this.showMessage(e.error.fieldsError[i],true)
        return EMPTY;
      }
    }
    this.showMessage("Ocorreu um erro!", true);
    return EMPTY;
  }

}
