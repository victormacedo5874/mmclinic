import { Address } from './../../shared/Address';
export class Company {
    id?: string = null;
    name: string = null;
    tradeName: string = null;
    cnpjCpf:string;
    personType: string;
    phone:string;
    email: string;
    address: Address= new Address();
  }
  
