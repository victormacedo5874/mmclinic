import { BsModalRef } from 'ngx-bootstrap/modal';
import { City } from './../../../shared/City';
import { Address } from './../../../shared/Address';
import { CityService } from './../../../shared/city.service';
import { Registration } from './../../../shared/registration';
import { CompanyService } from './../company.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Company } from '../Company';

@Component({
  selector: 'app-company-crud',
  templateUrl: './company-crud.component.html',
  styleUrls: ['./company-crud.component.css']
})
export class CompanyCrudComponent extends Registration implements OnInit {
  
  isLoading = false;
  citys:Array<City> = new Array<City>();
  citySearch :string = '';
  ufSearch:string = '';
  company: Company = new Company();
  CNPJ_CPF: string;
  
  constructor(private companyService: CompanyService,
              private cityService: CityService,
              private router: Router,
              private actRoute: ActivatedRoute) {
                super()
               }

  ngOnInit() {
    this.isLoading = true;
  
    this.getCity();
    const id = this.actRoute.snapshot.paramMap.get('id');
    if(id){
      this.companyService.findById(id).subscribe((company)=>{
        this.company = company;
        this.citySearch = this.company.address.city.description+"-"+this.company.address.city.state.initials;
        this.setPersonType();
        this.isLoading = false;
      })
    }else{
      this.company.personType = 'F';
      this.setPersonType();
      this.isLoading = false;
    }

  }

  back(){
    this.router.navigate(['/company/'])
  }

  getCity(){
    this.cityService.findAll().subscribe((result: any) => {
      this.citys = result;
    }, err => {
      console.log(err);
    });
  }

  setPersonType(){
    if(this.company.personType == 'F'){
      this.CNPJ_CPF = 'CPF';
    }else{
    this.CNPJ_CPF = 'CNPJ';
    }
  }

  getCityByName(){
    this.company.address.city = new City();
    let descriptionCity = this.citySearch.split("-");
    this.cityService.findCity(descriptionCity[0], descriptionCity[1]).subscribe((result: any) => {
      if(result.length == 1){
        this.company.address.city = result[0];
      }
    }, err => {
      console.log(err);
    });
  }

  create(){
    let validate = this.validateCompany();
    if(validate== null){
      this.companyService.create(this.company).subscribe(()=>{
        this.companyService.showMessage("Registro salvo com sucesso");
        this.router.navigate(['/company'])
      })
    }else(
      
      this.companyService.showMessage("Cnpj/Cpf já cadastrado.", true)
    )
  }

  validateCompany(){
    let response = null;
    this.companyService.findAll().subscribe(result => {
      let companies = <Array<Company>>result;
      companies.forEach(c => {
        if(c.cnpjCpf == this.company.cnpjCpf && c.id != this.company.id){
          response = "Cnpj/Cpf já cadastrado."
        }
      });
      return response; 
    })

  }

  selectRow(line:any){
    this.rowSelected = line;
  }

  getCep(company: Company){
    this.cityService.externalGet("https://viacep.com.br/ws/"+company.address.zipcode +"/json/").subscribe((result:any) =>{
      company.address.neighborhood = result.bairro;
      company.address.street = result.logradouro;
      company.address.complement = result.complemento;
      // this.citySearch = result.localidade
      this.ufSearch = result.uf;
      this.citySearch= result.localidade+"-"+result.uf;
      this.getCityByName();
      // this.getCityByName(address  );
    })
   }

}
