import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { Registration } from './../../../shared/registration';
import { CompanyService } from './../company.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { Company } from '../Company';

@Component({
  selector: 'app-company-read',
  templateUrl: './company-read.component.html',
  styleUrls: ['./company-read.component.css']
})
export class CompanyReadComponent extends Registration implements OnInit {

  profileFilter = 10;
  qtdeView = 5;
  isLoading = false;
  companies: Array<Company>;
  companiesFilter: Array<Company>;
  filter:string = "";
  itemsPerPage = 10;

  @ViewChild('modalConfirmation', { static: true }) template: TemplateRef<any>;
  constructor(private companyService: CompanyService,
              private router: Router,
              private modalService: BsModalService) {
                super()
               }

            
  ngOnInit() {
    this.isLoading = true;
    this.companyService.findAll().subscribe(
          result => {
          this.isLoading = false;
          this.companies = result;
          this.companiesFilter = this.companies;
    })

  }

  remove(){
    this.companyService.delete(this.rowSelected.id).subscribe(() => {
      this.companyService.showMessage("Registro excluido com sucesso");
      this.modalRef.hide()
      this.ngOnInit();
    });
  }

  callRemove(){
    this.msgModal= "Gostaria realmente de remover esse registro?";
    this.modalRef = this.modalService.show(this.template,{ class: ''});
  }

  edit(){
    this.router.navigate(['/company/crud/',this.rowSelected.id])
  }

  create(): void {
    this.router.navigate(['/company/crud'])
  }

  delete(company: Company) {
    console.log(company);
    this.companyService.delete(company.id).subscribe(() => {
      this.companyService.showMessage("Registro excluido com sucesso");
      this.ngOnInit();
    });
  }

  onSorting(event:any){
    this.ngOnInit()
  }

  changeFilter(event:any){
    if(this.filter){
      this.companiesFilter = this.companies.filter(item => {
        if(item.email == null){
          return  item.id.toLowerCase().match(this.filter)  || 
          item.name.toLowerCase().match(this.filter)  || 
          item.tradeName.toLowerCase().match(this.filter) || 
          item.personType.toLowerCase().match(this.filter) ||
          item.phone.toLowerCase().match(this.filter) ||
          item.cnpjCpf.toLowerCase().match(this.filter);
        }else{
          return  item.id.toLowerCase().match(this.filter)  || 
                  item.name.toLowerCase().match(this.filter)  || 
                  item.tradeName.toLowerCase().match(this.filter) || 
                  item.personType.toLowerCase().match(this.filter) ||
                  item.phone.toLowerCase().match(this.filter) ||
                  item.email.toLowerCase().match(this.filter) ||
                  item.cnpjCpf.toLowerCase().match(this.filter);

        }
      });
    }else{
      this.companiesFilter =  this.companies;
    }
  }
  
  paginate(){

  }

}
