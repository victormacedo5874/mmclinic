import { Company } from './Company';
import { endPoint } from './../../../main';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, EMPTY } from "rxjs";
import { map, catchError } from "rxjs/operators";
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: "root",
})
export class CompanyService {
  url =`${endPoint}`+"company";
  isLoading = false;
  durationInSeconds = 5;
  company:Array<Company> = new Array<Company>();

  constructor(private http: HttpClient, private snackBar: MatSnackBar) {}

  showMessage(msg: string, isError: boolean = false): void {
    console.log(msg)
    this.snackBar.open(msg, "X", {
      duration: 2000,
      horizontalPosition: "center",
      verticalPosition: "bottom",
      panelClass: isError ? ["msg-error"] : ["msg-success"],
    });
  }


  create(company: Company): Observable<Company> {
    // company.parckingId = this.sessionService.currentCompany.id;
    return this.http.post<Company>(this.url, company).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  read(): Observable<Company[]> {
    return this.http.get<Company[]>(this.url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  findAll(): Observable<any> {
    return this.http.get<Company>(this.url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  delete(id: string): Observable<Company> {
    const url = this.url+`/${id}`;
    return this.http.delete<Company>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  findById(id: string): Observable<Company> {
    this.isLoading = true;
    const url =  this.url+`/${id}`;
    return this.http.get<Company>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  errorHandler(e: any): Observable<any> {
    if(e.error && e.error.fieldsError){
      for(let i in e.error.fieldsError){
        this.showMessage(e.error.fieldsError[i],true)
        return EMPTY;
      }
    }
    this.showMessage("Ocorreu um erro!", true);
    return EMPTY;
  }
}
