import { EntityType } from './../../enum/EntityType';
import { Address } from '../../shared/Address';
export class Person {
    id?: string = null;
    nr: number = null;
    name: string = null;
    tradeName: string = null;
    cnpjCpf:string;
    personType: string;
    phone:string;
    email: string;
    address: Address= new Address();
    companyId: string;
    entityType: EntityType;
  }
  
