
import { EntityType } from './../../../enum/EntityType';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { City } from '../../../shared/City';
import { Address } from '../../../shared/Address';
import { CityService } from '../../../shared/city.service';
import { Registration } from '../../../shared/registration';
import { PersonService } from '../person.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Person } from '../Person';

@Component({
  selector: 'app-person-crud',
  templateUrl: './person-crud.component.html',
  styleUrls: ['./person-crud.component.css']
})
export class PersonCrudComponent extends Registration implements OnInit {
  
  isLoading = false;
  citys:Array<City> = new Array<City>();
  citySearch :string = '';
  ufSearch:string = '';
  person: Person = new Person();
  CNPJ_CPF: string;
  title: string;
  snapshot: RouterStateSnapshot;
  call:string;

  constructor(private personService: PersonService,
              private cityService: CityService,
              private router: Router,
              private actRoute: ActivatedRoute) {
                super()
                
               }

  ngOnInit() {
    this.isLoading = true;
    this.validUrl();
    this.getCity();
    const id = this.actRoute.snapshot.paramMap.get('id');
    if(id){

      if(this.person.personType == 'Fornecedor'){
        this.title = 'Fornecedor';
        this.call =  '/person/provider';
      }else{
        this.title = 'Cliente';
        this.call =  '/person/client';
      } 

      this.personService.findById(id).subscribe((person)=>{
        this.person = person;
        this.citySearch = this.person.address.city.description+"-"+this.person.address.city.state.initials;
        this.isLoading = false;
      })
    }else{
      this.person.personType = 'F';
      this.isLoading = false;
    }
  }

  validUrl(){
    this.snapshot = this.router.routerState.snapshot;
    if(this.snapshot.url == '/person/provider/crud'){
      this.title = 'Fornecedor';
      this.call =  '/person/provider';
      this.person.entityType = EntityType.PROVIDER;
    }else{
      this.title = 'Cliente';
      this.call =  '/person/client';
      this.person.entityType = EntityType.CLIENT;
    } 
  }

  back(){
    this.router.navigate([this.call])
  }

  getCity(){
    this.cityService.findAll().subscribe((result: any) => {
      this.citys = result;
    }, err => {
      console.log(err);
    });
  }

  getCityByName(){
    this.person.address.city = new City();
    let descriptionCity = this.citySearch.split("-");
    this.cityService.findCity(descriptionCity[0], descriptionCity[1]).subscribe((result: any) => {
      if(result.length == 1){
        this.person.address.city = result[0];
      }
    }, err => {
      console.log(err);
    });
  }

  create(){
    this.personService.create(this.person).subscribe(()=>{
      this.personService.showMessage("Registro salvo com sucesso");
      this.router.navigate([this.call])
    })
  }

  maskPhone(document:string):string{
    var   mask =  '(00)00000-0000'
    if(!document)
    return;
    // if(document.length >= 11){
    //   mask =  '(00)00000-0000'
    // }
    // if(document.length <=10){
    //   mask =  '(00)0000-0000'
    // }
    return mask;
  }


  selectRow(line:any){
    this.rowSelected = line;
  }

  getCep(person: Person){
    this.cityService.externalGet("https://viacep.com.br/ws/"+person.address.zipcode +"/json/").subscribe((result:any) =>{
      person.address.neighborhood = result.bairro;
      person.address.street = result.logradouro;
      person.address.complement = result.complemento;
      // this.citySearch = result.localidade
      this.ufSearch = result.uf;
      this.citySearch= result.localidade+"-"+result.uf;
      this.getCityByName();
      // this.getCityByName(address  );
    })
   }

}
