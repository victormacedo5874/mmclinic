import { PageableResponse } from './../../../shared/PageableResponse';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { Registration } from '../../../shared/registration';
import { PersonService } from '../person.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router, RouterStateSnapshot } from '@angular/router';
import { Person } from '../Person';
import { Pageable } from 'src/app/shared/Pageable';

@Component({
  selector: 'app-person-read',
  templateUrl: './person-read.component.html',
  styleUrls: ['./person-read.component.css']
})
export class PersonReadComponent extends Registration implements OnInit {

  profileFilter = 10;
  qtdeView = 5;
  isLoading = false;
  listPerson: Array<Person>;
  listPersonFilter: Array<Person>;
  filter:string = "";
  itemsPerPage = 10;
  title: string;
  snapshot: RouterStateSnapshot;
  pagebleResponse$: Subject<PageableResponse> = new Subject<PageableResponse>();
  pageable:Pageable = {page:0, direction:"asc", size: 30, sort:"name"}

  @ViewChild('modalConfirmation', { static: true }) template: TemplateRef<any>;
  constructor(private personService: PersonService,
              private router: Router,
              private modalService: BsModalService) {
                super()
               }

            
  ngOnInit() {
    this.snapshot = this.router.routerState.snapshot;
    if(this.snapshot.url == '/person/provider'){
      this.title = 'Fornecedor';
    }else{
      this.title = 'Cliente';
    }

    this.isLoading = true;
    this.personService.findAll(this.pageable, "",this.title).subscribe((pageableResponse: PageableResponse)  => {
          this.isLoading = false;
          this.listPerson = pageableResponse.content;
          this.listPersonFilter = this.listPerson;
    })

  }

  remove(){
    this.personService.delete(this.rowSelected.id).subscribe(() => {
      this.personService.showMessage("Registro excluido com sucesso");
      this.modalRef.hide()
      this.ngOnInit();
    });
  }

  callRemove(){
    this.msgModal= "Gostaria realmente de remover esse registro?";
    this.modalRef = this.modalService.show(this.template,{ class: ''});
  }

  edit(){
    this.router.navigate([this.snapshot.url+'/crud/',this.rowSelected.id])
  }

  create(): void {
    this.router.navigate([this.snapshot.url+'/crud'])
  }

  delete(person: Person) {
    console.log(person);
    this.personService.delete(person.id).subscribe(() => {
      this.personService.showMessage("Registro excluido com sucesso");
      this.ngOnInit();
    });
  }

  onSorting(event:any){
    this.ngOnInit()
  }

  changeFilter(event:any){
    if(this.filter){
      this.listPersonFilter = this.listPerson.filter(item => {
        if(item.email == null){
          return  item.id.toLowerCase().match(this.filter)  || 
          item.name.toLowerCase().match(this.filter)  || 
          item.tradeName.toLowerCase().match(this.filter) || 
          item.personType.toLowerCase().match(this.filter) ||
          item.phone.toLowerCase().match(this.filter) ||
          item.cnpjCpf.toLowerCase().match(this.filter);
        }else{
          return  item.id.toLowerCase().match(this.filter)  || 
                  item.name.toLowerCase().match(this.filter)  || 
                  item.tradeName.toLowerCase().match(this.filter) || 
                  item.personType.toLowerCase().match(this.filter) ||
                  item.phone.toLowerCase().match(this.filter) ||
                  item.email.toLowerCase().match(this.filter) ||
                  item.cnpjCpf.toLowerCase().match(this.filter);

        }
      });
    }else{
      this.listPersonFilter =  this.listPerson;
    }
  }
  
  paginate(){

  }

}
