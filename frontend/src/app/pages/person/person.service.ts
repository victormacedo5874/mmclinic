import { Pageable } from 'src/app/shared/Pageable';
import { PageableResponse } from './../../shared/PageableResponse';
import { Person } from './Person';
import { endPoint } from '../../../main';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, EMPTY } from "rxjs";
import { map, catchError } from "rxjs/operators";
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: "root",
})
export class PersonService {
  url =`${endPoint}`+"person";
  isLoading = false;
  durationInSeconds = 5;
  person:Array<Person> = new Array<Person>();
  companyId= '3efd1e607498416e9897d4303844b06f';
  pageable = new Pageable();

  constructor(private http: HttpClient, private snackBar: MatSnackBar) {}

  showMessage(msg: string, isError: boolean = false): void {
    console.log(msg)
    this.snackBar.open(msg, "X", {
      duration: 2000,
      horizontalPosition: "center",
      verticalPosition: "bottom",
      panelClass: isError ? ["msg-error"] : ["msg-success"],
    });
  }


  create(person: Person): Observable<Person> {
    person.companyId = this.companyId;
    return this.http.post<Person>(this.url, person).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  read(): Observable<Person[]> {
    return this.http.get<Person[]>(this.url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  documentExists(cnpjCpf: string, idPerson: string): Observable<Boolean> {
    this.isLoading = true;
    let query:URLSearchParams = new URLSearchParams();
    query.set('idCompany', '3efd1e607498416e9897d4303844b06f');
    query.set('cnpjCpf',cnpjCpf);
    query.set('idPerson',idPerson);
    return this.http.get<Boolean>(`${this.url}/document-exists?${query}`).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  findAll(pageable:Pageable, filter:string, entityType:string): Observable<PageableResponse> {
    var pagebleParameter = `&page=${pageable.page}&size=${pageable.size}&sort=${pageable.sort},${pageable.direction}`
    var url = this.url+`?filter=${filter}&entityType=`+entityType+`&companyId=${this.companyId}${pagebleParameter}` 
    return this.http.get<PageableResponse>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  findByDocument(cnpjCpf:string): Observable<Person> {
    let query:URLSearchParams = new URLSearchParams();
    query.set('idCompany', '3efd1e607498416e9897d4303844b06f');
    query.set('cnpjCpf',cnpjCpf);
    return this.http.get<Person>(`${this.url}/find-by-document?${query}`).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  delete(id: string): Observable<Person> {
    const url = this.url+`/${id}`;
    return this.http.delete<Person>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  findById(id: string): Observable<Person> {
    this.isLoading = true;
    const url =  this.url+`/${id}`;
    return this.http.get<Person>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  errorHandler(e: any): Observable<any> {
    if(e.error && e.error.fieldsError){
      for(let i in e.error.fieldsError){
        this.showMessage(e.error.fieldsError[i],true)
        return EMPTY;
      }
    }
    this.showMessage(e.error.message || "Ocorreu um erro!", true);
    return EMPTY;
  }

  validCpfCnpj(val) {
      if (val.length == 14 || val.length == 11) {
        let cpf = val.replace(/[^\d]+/g, '');
        if (cpf == '') return false;
        // Elimina CPFs invalidos conhecidos    
        if (cpf.length != 11 ||
          cpf == "11111111111" ||
          cpf == "22222222222" ||
          cpf == "33333333333" ||
          cpf == "44444444444" ||
          cpf == "55555555555" ||
          cpf == "66666666666" ||
          cpf == "77777777777" ||
          cpf == "88888888888" ||
          cpf == "99999999999")
          return false;
        // Valida 1o digito 
        let add = 0;
        for (let i = 0; i < 9; i++)
          add += parseInt(cpf.charAt(i)) * (10 - i);
        let rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
          rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
          return false;
        // Valida 2o digito 
        add = 0;
        for (let i = 0; i < 10; i++)
          add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
          rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
          return false;
        return true;
      } else if (val.length == 18 || val.length == 14) {
        let cnpj = val.trim();
        if (cnpj == null) {
          return false;
        }
        cnpj = cnpj.replace(/[^\d]+/g, '');

        if (cnpj == '') return false;

        if (cnpj.length != 14)
          return false;

        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "11111111111111" ||
          cnpj == "22222222222222" ||
          cnpj == "33333333333333" ||
          cnpj == "44444444444444" ||
          cnpj == "55555555555555" ||
          cnpj == "66666666666666" ||
          cnpj == "77777777777777" ||
          cnpj == "88888888888888" ||
          cnpj == "99999999999999")
          return false;

        // Valida DVs
        length = cnpj.length - 2
        let number = cnpj.substring(0, length);
        let character = cnpj.substring(length);
        let sum = 0;
        let pos = length - 7;
        for (let i = length; i >= 1; i--) {
          sum += number.charAt(length - i) * pos--;
          if (pos < 2)
            pos = 9;
        }
        let resultado = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (resultado != character.charAt(0))
          return false;

        length = length + 1;
        number = cnpj.substring(0, length);
        sum = 0;
        pos = length - 7;
        for (let i = length; i >= 1; i--) {
          sum += number.charAt(length - i) * pos--;
          if (pos < 2)
            pos = 9;
        }
        resultado = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (resultado != character.charAt(1))
          return false;

        return true;
      }
    }

}
