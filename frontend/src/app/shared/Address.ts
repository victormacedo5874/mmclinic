import { City } from './City';
export class Address {
    id?: string = null;
    zipcode: string = null;
    streetNumber: string  = null;
    neighborhood: string = null;
    street: string = null;
    complement: string = null;
    city: City = new City();
  }
  
