import { State } from './State';
export class City {
    id?: string = null;
    description: string = null;
    ibge: string = null;
    state: State = new State();
  }
  
