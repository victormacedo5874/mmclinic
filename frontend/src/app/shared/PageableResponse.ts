class Sort {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
}

class Pageable {
    sort: Sort;
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    unpaged: boolean;
}

export class PageableResponse {
    content: any[];
    pageable: Pageable;
    totalElements: number;
    totalPages: number;
    last: boolean;
    number: number;
    size: number;
    sort: Sort;
    numberOfElements: number;
    first: boolean;
    empty: boolean;
}