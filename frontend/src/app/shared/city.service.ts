import { City } from './City';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, EMPTY } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { endPoint } from 'src/main';

@Injectable({
  providedIn: "root",
})
export class CityService {
  url =`${endPoint}`+"city";
  isLoading = false;
  City:Array<City> = new Array<City>();

  constructor(private http: HttpClient) {}

  create(City: City): Observable<City> {
    // City.parckingId = this.sessionService.currentCompany.id;
    return this.http.post<City>(this.url, City).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  read(): Observable<City[]> {
    return this.http.get<City[]>(this.url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  findCity(description:string, uf:string): Observable<any> {
    return this.http.get<City>(this.url+'/byDescription?description='+description + '&uf=' + uf).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  findAll(): Observable<any> {
    return this.http.get<City>(this.url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  delete(id: string): Observable<City> {
    const url = this.url+`/${id}`;
    return this.http.delete<City>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  findById(id: string): Observable<City> {
    this.isLoading = true;
    const url =  this.url+`/${id}`;
    return this.http.get<City>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  
  errorHandler(e: any): Observable<any> {
    console.log(e)
    if(e.error && e.error.fieldsError){
      for(let i in e.error.fieldsError){
        return EMPTY;
      }
    }
    return EMPTY;
  }

  public externalGet(url:string){
    //this.loading = true;
    return this.http.get(url);
  }
}
