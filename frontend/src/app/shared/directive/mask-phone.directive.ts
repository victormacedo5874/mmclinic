import { Directive, ElementRef, forwardRef, Renderer2 } from '@angular/core';
import { NG_VALUE_ACCESSOR, DefaultValueAccessor, NgModel } from '@angular/forms';

const PROVIDER = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => MaskPhoneDirective),
  multi: true,
};

@Directive({
  selector: 'input[maskPhone]',
  host: {
    // When the user updates the input
    '(keyup)': 'onInput($event)',
    '(blur)': 'onTouched()',
  },
  providers: [
    PROVIDER,
  ],
})
export class MaskPhoneDirective extends DefaultValueAccessor {

  constructor(renderer: Renderer2, elementRef: ElementRef) {
      super(renderer, elementRef, false);
  }
  writeValue(value: any): void {
    const transformed = this.transformValue(value);

    super.writeValue(transformed);
  }

  onInput(event: any): void {
    const transformed = this.transformValue(event);

    super.writeValue(transformed);
    this.onChange(transformed);
  }

  private transformValue(event: any): any {
    if(!event)
      return;
    
    if(!event.target){
      event = {target:{value: event}};
    }

    var value = event.target.value;

    if (event.keyCode == 8) {
      return value;
    } 

    var value = value.replace(/\D/g, '');

    if(value.length == 0){
      return;
    }

    let result = ""
    // don't show braces for empty groups at the end
    if (value.length == 2) {
      result = value.replace(/^(\d{0,2})/, '($1)');
    }else if (value.length <= 10) {
      result = value.replace(/^(\d{0,2})(\d{0,4})(\d{0,4})/, '($1) $2-$3');
    } else {
      result = value.replace(/^(\d{0,2})(\d{0,5})(\d{0,4})/, '($1) $2-$3');
    }
    return result;
  }

}
