import { Component, ContentChild, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControlName, FormGroup, NgModel } from '@angular/forms';

@Component({
  selector: 'app-input-container',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.css']
})
export class InputContainerComponent implements OnInit {

  @Input('label')
  label:string
  @Input('messageError')
  messageError:string
  @Input('size')
  size:string
  
  input: any; 
  @ContentChild(NgModel) model: NgModel
  @ContentChild(FormControlName) control: FormControlName;

  inputElement:any

  constructor(private _elRef:ElementRef) {}

  ngAfterViewInit() {
  }

  ngAfterContentInit() {
    this.inputElement = this._elRef.nativeElement.querySelector('input');
    this.input = this.model || this.control;
    
    if(this.input === undefined){
      throw new Error('Esse componente precisa ser usado com uma diretiva ngModel ou formControlName');
    }
  }

  ngOnInit() {
    
  }

  hasError(): boolean {
    let hasError = this.input.invalid && (this.input.dirty || this.input.touched || this.input.formDirective.submitted)
    
    if(hasError){

      if(this.input.errors && !this.messageError){
        if(this.input.errors.required){
          this.messageError = `Campo ${this.label} obrigatório`;
        }
      }

      this.inputElement.classList.add('is-invalid')
      this.inputElement.classList.remove('is-valid')
    }else if(this.input.valid && this.input.control.validator){
      this.inputElement.classList.remove('is-invalid')
      this.inputElement.classList.add('is-valid')
    }
    return hasError
  }


}
