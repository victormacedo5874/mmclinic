import { BsModalRef } from 'ngx-bootstrap/modal';
import { environment } from './../../environments/environment';

export class Registration{
    
    public nullValue      = null;

    public topicName      : String;
    public currentPage    = 1;
    public itemsPerPage   : number;
    public totalPag       : number;
    public rowSelected    : any;

    public msgError       : String = "";
    public msgModal       : String;

    public field:string    = '';
    public reverse:boolean = false;
    
    public typeView:string = 'SHOW_DATA';
    //public sharedService:SharedService;
    public modalRef : BsModalRef;

    public link= environment.endPoint;
    public p;

    public qtdeView = 10;

    constructor(){
      //  this.sharedService = SharedService.getInstance();
      //  this.sharedService.screen = screen;
    }

    include():void{
        this.typeView = 'SHOW_DATA';
    };
    
    setItensPerPag(qtdePag:number){
        this.itemsPerPage = qtdePag;
    }

    selectRow(line:any){
        this.rowSelected = line;
    }

    Paginacao(totalPag){
        this.totalPag = totalPag;
    }

    public setOrder(value: string) {
        if (this.field == value) {
            this.reverse = !this.reverse;
        }
        this.field = value;
    }

    compare( o1, o2 ) : boolean {
        if(o1 === o2){
            return true
        }
        if(o1 && o2)
            return o1.id === o2.id;
    }

    paintTable = function(obj){
        let style;
    
        if (this.rowSelected == obj){ 
          style = "table-info";
        } else{
          style = null;
        }
        return style;
      }

}
