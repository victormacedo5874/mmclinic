import { MaskPhoneDirective } from './directive/mask-phone.directive';

import { ViewTotalPageComponent } from './view-total-page/view-total-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputContainerComponent } from './input-container/input-container.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [InputContainerComponent, ViewTotalPageComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [InputContainerComponent, ViewTotalPageComponent]
})
export class SharedModule { }
