import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-view-total-page',
  templateUrl: './view-total-page.component.html',
  styleUrls: ['./view-total-page.component.css']
})
export class ViewTotalPageComponent implements OnInit {

  
  @Input() listaFiltro;
  @Output() rated = new EventEmitter<number>();
  @Output() ratedPag = new EventEmitter<number>();

  totalPag : number;
  qtdeView : number;
  constructor() { }

  ngOnInit() {
    this.qtdeView = 10;
    this.setItemsPerPage(this.qtdeView);
  }

  setItemsPerPage(qtd) {
    this.qtdeView = qtd;
    this.rated.emit(qtd);
    if(this.listaFiltro == null){
      this.listaFiltro = [];
    }
    this.totalPag = Math.round(this.listaFiltro.length/ qtd);
    if(this.totalPag <= 0){
     this.totalPag = 1;
    }
    this.ratedPag.emit(this.totalPag);
  }
  
  paint(qtd){
    if(this.qtdeView == qtd){
      return "mark";
    }
  }
}
