import { CompanyCrudComponent } from './../../pages/company/company-crud/company-crud.component';
import { CompanyComponent } from './company.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [ {path: '', component: CompanyComponent},
                         {path: 'crud', component: CompanyCrudComponent},
                         { path: 'crud/:id', component: CompanyCrudComponent}
                      ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
