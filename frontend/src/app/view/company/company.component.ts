import { CompanyService } from './../../pages/company/company.service';
import { Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  filter$:Subject<string> = new Subject<string>();
  constructor(private router: Router, private companyService: CompanyService) {
    /*companyService.headerData = {
      title: 'Cadastro de Período',
      icon: 'directions_car',
      routeUrl: '/companys'
    }*/
  }

  ngOnInit(): void {
  }

  changeFilter(filter){
    this.filter$.next(filter)
  }
}
