import { SharedModule } from './../../shared/shared.module';
import { ViewTotalPageComponent } from './../../shared/view-total-page/view-total-page.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CityService } from './../../shared/city.service';
import { CompanyCrudComponent } from './../../pages/company/company-crud/company-crud.component';
import { BrowserModule } from '@angular/platform-browser';
import { CompanyReadComponent } from './../../pages/company/company-read/company-read.component';
import { CompanyRoutingModule } from './company-routing.module';
import { CompanyService } from './../../pages/company/company.service';
import { CompanyComponent } from './company.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxMaskModule } from 'ngx-mask';
import { OrderModule } from 'ngx-order-pipe';

@NgModule({
  declarations: [CompanyComponent,CompanyReadComponent, CompanyCrudComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    CompanyRoutingModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    ModalModule.forRoot(),
    NgxPaginationModule,
    OrderModule,
    SharedModule
  ],
  providers:[
    CompanyService,
    CityService
  ],
})
export class CompanyModule { }
