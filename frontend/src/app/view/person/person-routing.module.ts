import { PersonCrudComponent } from '../../pages/person/person-crud/person-crud.component';
import { PersonComponent } from './person.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [ {path: '', component: PersonComponent},
                         {path: 'crud', component: PersonCrudComponent},
                         { path: 'crud/:id', component: PersonCrudComponent}
                      ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonRoutingModule { }
