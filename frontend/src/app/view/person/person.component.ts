import { PersonService } from '../../pages/person/person.service';
import { Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router , RouterStateSnapshot} from '@angular/router';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  title: string;
  filter$:Subject<string> = new Subject<string>();
  constructor(private router: Router, private personService: PersonService) {
  }

  ngOnInit(): void {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    console.log(snapshot.url);
    if(snapshot.url == '/person/provider'){
      this.title = 'Fornecedor';
    }else{
      this.title = 'Cliente';
    }
    
  }

  changeFilter(filter){
    this.filter$.next(filter)
  }
}
