import { PersonReadComponent } from '../../pages/person/person-read/person-read.component';
import { SharedModule } from '../../shared/shared.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CityService } from '../../shared/city.service';
import { PersonCrudComponent } from '../../pages/person/person-crud/person-crud.component';
import { PersonRoutingModule } from './person-routing.module';
import { PersonService } from '../../pages/person/person.service';
import { PersonComponent } from './person.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxMaskModule } from 'ngx-mask';
import { OrderModule } from 'ngx-order-pipe';

@NgModule({
  declarations: [PersonComponent,PersonReadComponent, PersonCrudComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    PersonRoutingModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    ModalModule.forRoot(),
    NgxPaginationModule,
    OrderModule,
    SharedModule
  ],
  providers:[
    PersonService,
    CityService
  ],
})
export class PersonModule { }
